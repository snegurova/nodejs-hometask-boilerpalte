const { Router } = require('express');
const UserService = require('../services/userService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');

const router = Router();

// TODO: Implement route controllers for user

router.put('/:id', updateUserValid, (req, res, next) => {
  if (req.params.id) {
    if (res.isValid) {
      try {
        const user = UserService.update(req.params.id, req.body);
        res.data = user;
      } catch (err) {
        err.status = 400;
        res.err = err;
      } finally {
        next();
      }
    }
  } else {
    res.status(400).send({
      error: true,
      message: 'Id parameter is mandatory to update user'
    });
  }

}, responseMiddleware);

router.post('/', createUserValid, (req, res, next) => {
  if (res.isValid) {
    const { body: { firstName, lastName, email, phoneNumber, password } } = req;
    try {
      const user = UserService.add({firstName, lastName, email, phoneNumber, password});
      res.data = user;
    } catch (err) {
      err.status = 400;
      res.err = err;
    } finally {
      next();
    }
  }
}, responseMiddleware);

router.get('/:id?', (req, res, next) => {
  const { body: { email = '', password } } = req;
  if (req.params.id) {
    try {
      const user = UserService.getUser(req.params.id);
      res.data = user;
    } catch (err) {
      err.status = 404;
      res.err = err;
    } finally {
      next();
    }
  } else {
    try {
      const users = UserService.getAll();
      res.data = users;
    } catch (err) {
      err.status = 404;
      res.err = err;
    } finally {
      next();
    }
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  if (req.params.id) {
      try {
        const user = UserService.delete(req.params.id);
        res.data = user;
      } catch (err) {
        err.status = 400;
        res.err = err;
      } finally {
        next();
      }
  } else {
    res.status(400).send({
      error: true,
      message: 'Id parameter is mandatory to delete user'
    });
  }

}, responseMiddleware);

module.exports = router;