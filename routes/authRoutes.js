const {Router} = require('express');
const AuthService = require('../services/authService');
const {responseMiddleware} = require('../middlewares/response.middleware');

const router = Router();

router.post('/login', (req, res, next) => {
  try {
    // TODO: Implement login action (get the user if it exist with entered credentials)
    const { body: { email, password } } = req;
    const data = AuthService.login({ email, password });
    res.data = data;
  } catch (err) {
    err.status = 404;
    res.err = err;
  } finally {
    next();
  }
}, responseMiddleware);

module.exports = router;