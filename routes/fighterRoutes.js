const { Router } = require('express');
const FighterService = require('../services/fighterService');
const { responseMiddleware } = require('../middlewares/response.middleware');
const { createFighterValid, updateFighterValid } = require('../middlewares/fighter.validation.middleware');

const router = Router();

// TODO: Implement route controllers for fighter

router.post('/', createFighterValid, (req, res, next) => {
  if (res.isValid) {
    const { body: { name, health = 100, power, defense } } = req;
    try {
      const fighter = FighterService.add({name, health, power, defense});
      res.data = fighter;
    } catch (err) {
      err.status = 400;
      res.err = err;
    } finally {
      next();
    }
  }
}, responseMiddleware);

router.put('/:id', updateFighterValid, (req, res, next) => {
  if (req.params.id) {
    if (res.isValid) {
      try {
        const fighter = FighterService.update(req.params.id, req.body);
        res.data = fighter;
      } catch (err) {
        err.status = 400;
        res.err = err;
      } finally {
        next();
      }
    }
  } else {
    res.status(400).send({
      error: true,
      message: 'Id parameter is mandatory to update Fighter'
    });
  }

}, responseMiddleware);

router.get('/:id?', (req, res, next) => {
  const { body: { email = '', password } } = req;
  if (req.params.id) {
    try {
      const user = FighterService.getFighter(req.params.id);
      res.data = user;
    } catch (err) {
      err.status = 404;
      res.err = err;
    } finally {
      next();
    }
  } else {
    try {
      const users = FighterService.getAll();
      res.data = users;
    } catch (err) {
      err.status = 404;
      res.err = err;
    } finally {
      next();
    }
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  if (req.params.id) {
    try {
      const fighter = FighterService.delete(req.params.id);
      res.data = fighter;
    } catch (err) {
      err.status = 400;
      res.err = err;
    } finally {
      next();
    }
  } else {
    res.status(400).send({
      error: true,
      message: 'Id parameter is mandatory to delete fighter'
    });
  }

}, responseMiddleware);

module.exports = router;