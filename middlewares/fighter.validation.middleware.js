const { fighter } = require('../models/fighter');

const createFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during creation
    res.isValid = true;
    const mandatoryFields = Object.keys(fighter).filter(key => key !== 'id' && key !== 'health');
    for (let i = 0; i < mandatoryFields.length; i++) {
        const field = mandatoryFields[i];
        if (!req.body[ field ]) {
            res.isValid = false;
            res.status(400).send({
                error: true,
                message: 'Mandatory fields are absent for Fighter entity'
            });
            break;
        }
    }
    if (req.body[ 'id' ]) {
        res.isValid = false;
        res.status(400).send({
            error: true,
            message: 'Id field is forbidden for request body'
        });
    }
    const bodyFields = Object.keys(req.body);
    for (let i = 0; i < bodyFields.length; i++) {
        const field = bodyFields[i];
        if (!mandatoryFields.includes(field) && field !== 'health') {
            console.log(field);
            res.isValid = false;
            res.status(400).send({
                error: true,
                message: 'Extra fields are forbidden for request body'
            });
            break;
        }
    }
    if (req.body.power <= 1 || req.body.power >= 100) {
        res.isValid = false;
        res.status(400).send({
            error: true,
            message: 'Power should be in between 0 and 100'
        });
    }
    if (req.body.defense <= 1 || req.body.defense >= 10) {
        res.isValid = false;
        res.status(400).send({
            error: true,
            message: 'Defense should be in between 0 and 10'
        });
    }
    if (req.body.health <= 80 || req.body.health >= 120) {
        res.isValid = false;
        res.status(400).send({
            error: true,
            message: 'Health should be in between 80 and 120'
        });
    }

    next();
}

const updateFighterValid = (req, res, next) => {
    // TODO: Implement validatior for fighter entity during update
    res.isValid = true;
    const fields = Object.keys(fighter).filter(key => key !== 'id');
    const bodyFields = Object.keys(req.body);
    if (bodyFields.length === 0) {
        res.isValid = false;
        res.status(400).send({
            error: true,
            message: 'There is no properties to update for Fighter entity'
        });
    }

    for (let i = 0; i < bodyFields.length; i++) {
        const field = bodyFields[i];
        if (!fields.includes(field)) {
            res.isValid = false;
            res.status(400).send({
                error: true,
                message: 'Extra fields are forbidden for request body'
            });
            break;
        }
    }
    if (req.body[ 'id' ]) {
        res.isValid = false;
        res.status(400).send({
            error: true,
            message: 'Id field is forbidden for request body'
        });
    }
    if (req.body.power) {
        if (req.body.power <= 1 || req.body.power >= 100) {
            res.isValid = false;
            res.status(400).send({
                error: true,
                message: 'Power should be in between 0 and 100'
            });
        }
    }
    if (req.body.defense) {
        if (req.body.defense <= 1 || req.body.defense >= 10) {
            res.isValid = false;
            res.status(400).send({
                error: true,
                message: 'Defense should be in between 0 and 10'
            });
        }
    }
    if (req.body.health) {
        if (req.body.health <= 80 || req.body.health >= 120) {
            res.isValid = false;
            res.status(400).send({
                error: true,
                message: 'Health should be in between 80 and 120'
            });
        }
    }
    next();
}

exports.createFighterValid = createFighterValid;
exports.updateFighterValid = updateFighterValid;