const responseMiddleware = (req, res, next) => {
   // TODO: Implement middleware that returns result of the query
    if (res.data) {
        const data = res.data;
        const propsToDelete = ['password', 'id', 'createdAt'];
        const deleteProps = (data, propsToDelete) => {
            propsToDelete.forEach(prop => {
                if (data.hasOwnProperty(prop)) {
                    delete data[prop];
                }
            });
        };
        if (Array.isArray(data)) {
            data.forEach(el => {
                deleteProps(el, propsToDelete);
            });
        } else {
            deleteProps(data, propsToDelete);
        }

        res.status(200).send(data);
    }
    if (res.err) {
        const err = res.err;
        res.status(err.status).send({
            error: true,
            message: err.message
        });
    }

    next();
}

exports.responseMiddleware = responseMiddleware;