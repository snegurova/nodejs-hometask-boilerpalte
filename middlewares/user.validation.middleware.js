const {user} = require('../models/user');
const createUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during creation
  res.isValid = true;
  const mandatoryFields = Object.keys(user).filter(key => key !== 'id');
  for (let i = 0; i < mandatoryFields.length; i++) {
    const field = mandatoryFields[i];
    if (!req.body[ field ]) {
      res.isValid = false;
      res.status(400).send({
        error: true,
        message: 'Mandatory fields are absent for User entity'
      });
      break;
    }
  }
  if (req.body[ 'id' ]) {
    res.isValid = false;
    res.status(400).send({
      error: true,
      message: 'Id field is forbidden for request body'
    });
  }
  if (mandatoryFields.length < Object.keys(req.body).length) {
    res.isValid = false;
    res.status(400).send({
      error: true,
      message: 'Extra fields are forbidden for request body'
    });
  }
  if (!(/^[\w.+\-]+@gmail\.com$/i).test(req.body.email)) {
    res.isValid = false;
    res.status(400).send({
      error: true,
      message: 'Email is not gmail mailbox'
    });
  }
  if (!(/^\+380\d{9}/i).test(req.body.phoneNumber)) {
    res.isValid = false;
    res.status(400).send({
      error: true,
      message: 'Phone format is not +380xxxxxxxxx'
    });
  }
  if (req.body.password.length < 3) {
    res.isValid = false;
    res.status(400).send({
      error: true,
      message: 'Password is shorter then 3 characters'
    });
  }
  next();
};

const updateUserValid = (req, res, next) => {
  // TODO: Implement validatior for user entity during update
  res.isValid = true;
  const fields = Object.keys(user).filter(key => key !== 'id');
  if (Object.keys(req.body).length === 0) {
    res.isValid = false;
    res.status(400).send({
      error: true,
      message: 'There is no properties to update for User entity'
    });
  }
  const bodyFields = Object.keys(req.body);
  for (let i = 0; i < bodyFields.length; i++) {
    const field = bodyFields[i];
    if (!fields.includes(field)) {
      res.isValid = false;
      res.status(400).send({
        error: true,
        message: 'Extra fields are forbidden for request body'
      });
      break;
    }
  }
  for (let i = 0; i < fields.length; i++) {
    res.isValid = false;
    const field = fields[i];
    if (req.body[ field ]) {
      res.isValid = true;
      break;
    }
  }
  if (!res.isValid) {
    res.status(400).send({
      error: true,
      message: 'There is no properties to update for User entity'
    });
  }
  if (req.body[ 'id' ]) {
    res.isValid = false;
    res.status(400).send({
      error: true,
      message: 'Id field is forbidden for request body'
    });
  }
  if (req.body.email) {
    if (!(/^[\w.+\-]+@gmail\.com$/i).test(req.body.email)) {
      res.isValid = false;
      res.status(400).send({
        error: true,
        message: 'Email is not gmail mailbox'
      });
    }
  }
  if (req.body.phoneNumber) {
    if (!(/^\+380\d{9}/i).test(req.body.phoneNumber)) {
      res.isValid = false;
      res.status(400).send({
        error: true,
        message: 'Phone format is not +380xxxxxxxxx'
      });
    }
  }
  if (req.body.password) {
    if (req.body.password.length < 3) {
      res.isValid = false;
      res.status(400).send({
        error: true,
        message: 'Password is shorter then 3 characters'
      });
    }
  }
  next();
};

exports.createUserValid = createUserValid;
exports.updateUserValid = updateUserValid;