const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters
  add(data) {
    const nameExists = this.isFighterExists('name', data.name);
    if (nameExists) {
      throw Error('Fighter with this name already exists');
    }
    const user = FighterRepository.create(data);
    if (!user) {
      throw Error('There was a problem when creating User');
    }
    return user;
  }

  update(id, data) {
    if (!this.isFighterExists('id', id)) {
      throw Error('User is not found');
    }
    const user = FighterRepository.update(id, data);
    if (!user) {
      throw Error('There was a problem when updating User');
    }
    return user;
  }

  delete(id) {
    if (!this.isFighterExists('id', id)) {
      throw Error('Fighter is not found');
    }
    const fighter = FighterRepository.delete(id);
    if (!fighter) {
      throw Error('There was a problem when deleting Fighter');
    }
    return fighter;
  }

  isFighterExists(fieldName, fieldValue) {
    return FighterRepository.getAll().filter(user => user[fieldName] === fieldValue).length > 0
  }

  getFighter(id) {
    const fighter = FighterRepository.getAll().filter(fighter => fighter.id === id)[0];
    if (!fighter) {
      throw Error('There was a problem when creating Fighter');
    }
    return fighter;
  }

  getAll() {
    const fighters = FighterRepository.getAll();
    if (fighters.length === 0) {
      throw Error('There is no users');
    }
    return fighters;
  }
}

module.exports = new FighterService();