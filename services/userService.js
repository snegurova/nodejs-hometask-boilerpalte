const { UserRepository } = require('../repositories/userRepository');

class UserService {

    // TODO: Implement methods to work with user

    add(data) {
        const emailExists = this.isUserExists('email', data.email);
        if (emailExists) {
            throw Error('User with this email already exists');
        }
        const phoneExists = this.isUserExists('phoneNumber', data.phoneNumber);
        if (phoneExists) {
            throw Error('User with this phone number already exists');
        }
        const user = UserRepository.create(data);
        if (!user) {
            throw Error('There was a problem when creating User');
        }
        return user;
    }
    update(id, data) {
        if (!this.isUserExists('id', id)) {
            throw Error('User is not found');
        }
        const user = UserRepository.update(id, data);
        if (!user) {
            throw Error('There was a problem when updating User');
        }
        return user;
    }
    delete(id) {
        if (!this.isUserExists('id', id)) {
            throw Error('User is not found');
        }
        const user = UserRepository.delete(id);
        if (!user) {
            throw Error('There was a problem when deleting User');
        }
        return user;
    }
    isUserExists(fieldName, fieldValue) {
        return UserRepository.getAll().filter(user => user[fieldName] === fieldValue).length > 0
    }
    getUser(id) {
        const user = UserRepository.getAll().filter(user => user.id === id)[0];
        if (!user) {
            throw Error('There was a problem when creating User');
        }
        return user;
    }
    getAll() {
        const users = UserRepository.getAll();
        if (users.length === 0) {
            throw Error('There is no users');
        }
        return users;
    }

    search(search) {
        const item = UserRepository.getOne(search);
        if(!item) {
            throw Error('User not found');
        }
        return item;
    }
}

module.exports = new UserService();